export interface Plane {
    x: number;
    y: number;
}

export interface MousePlane {
    clientX: number;
    clientY: number;
}

export enum Actions {
    add = 0,
    remove,
    erase,
    move
}
