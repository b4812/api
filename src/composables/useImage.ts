import { ref } from 'vue';

export default function useImage() {
    const imgFile = ref<File | null>(null);
    const img = ref<HTMLImageElement | null>(null);
    const imgEdited = ref<HTMLImageElement | null>(null);

    function fileToImage(file: File | Blob): Promise<HTMLImageElement> {
        return new Promise((resolve) => {
            const reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = () => {
                const img = new Image();
                img.src = reader.result as string;
                img.onload = () => {
                    resolve(img);
                };
            };
        });
    }

    return { imgFile, img, imgEdited, fileToImage };
}
