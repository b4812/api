import type { Ref } from 'vue';

interface Plane {
    x: number;
    y: number;
}

export default function useTranslateImage(
    drawImage: () => void,
    redrawOnImageTranslated: () => void,
    img: Ref<HTMLImageElement | null>,
    startDragOffset: Ref<Plane>,
    translatePos: Ref<Plane>,
    scale: Ref<number>
) {
    let mouseDownInCanvas = false;

    function translateImageMouseDownListener({ x, y }: { x: number; y: number }) {
        mouseDownInCanvas = true;
        startDragOffset.value.x = x - translatePos.value.x;
        startDragOffset.value.y = y - translatePos.value.y;
    }

    function translateImageMouseUpListener() {
        mouseDownInCanvas = false;
    }

    function translateImageMouseOverListener() {
        mouseDownInCanvas = false;
    }
    function translateImageMouseOutListener() {
        mouseDownInCanvas = false;
    }
    function translateImageMouseMoveListener({ x, y }: { x: number; y: number }, cb? : () => void) {
        if (!mouseDownInCanvas || !img.value) return;
        translatePos.value.x = x - startDragOffset.value.x;
        translatePos.value.y = y - startDragOffset.value.y;

        if (translatePos.value.x > 400) {
            translatePos.value.x = 400;
        } else if (translatePos.value.x < -img.value.width * scale.value + 400) {
            translatePos.value.x = -img.value.width * scale.value + 400;
        }

        if (translatePos.value.y > 600) {
            translatePos.value.y = 600;
        } else if (translatePos.value.y < -img.value.height * scale.value + 300) {
            translatePos.value.y = -img.value.height * scale.value + 300;
        }
        drawImage();
        redrawOnImageTranslated()
    }

    return {
        translateImageMouseDownListener,
        translateImageMouseUpListener,
        translateImageMouseOverListener,
        translateImageMouseOutListener,
        translateImageMouseMoveListener,
    };
}
