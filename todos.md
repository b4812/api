* Hacer que el zoomIn y zoomOut sean suaves             ✓
* Poner un límite máximo para el zoomIn y zoomOut       ✓
* Toggle buttons para verde, rojo, borrar y mover
* Añadir atajos para el toggle button
* Pintar en el canvas ya sea verde o rojo
* Borrar en el canvas
* Que el tamaño de la brocha o borrador escale
* Cambiar el puntero dependiendo del toggle button
