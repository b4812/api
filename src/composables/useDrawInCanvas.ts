import type { Ref } from 'vue';
import type { Plane } from '@/types';
import { Actions } from '@/types';

interface Strokes {
    [k: string]: {
        points: Plane[];
        scale: number;
        translatePos: Plane;
        type: Actions;
        style: {
            width: number;
            color: string;
        };
    }
}

export default function useDrawInCanvas(
    inputCtx: Ref<CanvasRenderingContext2D | null>,
    outputCtx: Ref<CanvasRenderingContext2D | null>,
    options: Ref<Actions>,
    scale: Ref<number>,
    translatePos: Ref<Plane>
) {
    const MAX_WIDTH = 70
    const strokes: Strokes = {};
    let strokeKey = -1;
    let mouseDownDraw = false;
    let prevX = 0;
    let prevY = 0;

    function getColor(action: Actions) {
        if (action === Actions.add) {
            return 'rgba(0, 255, 0, 1)'
        } else {
            return 'rgba(255, 0, 0, 1)'
        }
    }

    function setPrevPoint(x: number, y: number) {
        prevX = x
        prevY = y
    }

    function drawStroke(ctx: CanvasRenderingContext2D, x: number, y: number, width: number, color: string, setPrev = false) {
        const newX = Math.floor((x - translatePos.value.x) / scale.value) * scale.value
        const newY = Math.floor((y - translatePos.value.y) / scale.value) * scale.value
        ctx.beginPath()
        ctx.moveTo(prevX, prevY)
        ctx.strokeStyle = color
        ctx.lineWidth = width
        ctx.lineCap = 'square'
        ctx.lineTo(newX, newY)
        ctx.stroke()
        ctx.closePath()
        if (setPrev) {
            setPrevPoint(newX, newY)
        }
    }

    function redrawOnImageTranslated() {
        const vals = Object.values(strokes);
        vals.forEach((v) => {
            if (!v.points.length) return
            const setX = (x: number) => (x) * scale.value / v.scale + translatePos.value.x
            const setY = (y: number) => (y) * scale.value / v.scale + translatePos.value.y
            setPrevPoint(setX(v.points[0].x), setY(v.points[0].y))
            v.points.forEach((point) => {
                if (v.type === Actions.add || v.type === Actions.remove) {
                    drawStroke( inputCtx.value!, setX(point.x), setY(point.y), v.style.width * scale.value / v.scale, v.style.color, true);
                } else {
                    drawErase(setX(point.x), setY(point.y), v.style.width * scale.value / v.scale)
                }
            });
        });
    }

    function drawErase(x: number, y: number, width: number) {
        if (!(inputCtx.value && outputCtx.value)) return
        const canvasX = Math.floor(x - width / 2)
        const canvasY = Math.floor(y - width / 2)
        inputCtx.value.drawImage(outputCtx.value.canvas, canvasX, canvasY, width, width, canvasX, canvasY, width, width)
    }

    function drawMouseDownListener({ x, y }: Plane) {
        mouseDownDraw = true;
        const newX = Math.floor((x - translatePos.value.x) / scale.value) * scale.value
        const newY = Math.floor((y - translatePos.value.y) / scale.value) * scale.value
        // setPrevPoint(newX, newY)
        setPrevPoint(x, y)
        strokeKey++;
        strokes[`${strokeKey}`] = {
            points: [],
            scale: scale.value,
            translatePos: {...translatePos.value},
            type: options.value,
            style: {
                width: 70 - (70 - scale.value) / 1.5,
                color: getColor(options.value)
            }
        };
    }

    function drawMouseUpListener() {
        mouseDownDraw = false;
    }

    function drawMouseOverListener() {
        mouseDownDraw = false;
    }

    function drawMouseOutListener() {
        mouseDownDraw = false;
    }

    function drawMouseMoveListener({ x, y }: Plane) {
        if (!(inputCtx.value && outputCtx.value && mouseDownDraw === true)) return;
        
        // console.log(Math.floor((x - translatePos.value.x) / scale.value) * scale.value, Math.floor((y - translatePos.value.y) / scale.value) * scale.value)
        // const newX = Math.floor((x - translatePos.value.x) / scale.value) * scale.value
        // const newY = Math.floor((y - translatePos.value.y) / scale.value) * scale.value
        // // console.log(strokes[`${strokeKey}`].style.width / scale.value)
        // const width = strokes[`${strokeKey}`].style.width 
        // if (options.value === Actions.add || options.value === Actions.remove) {
        //     drawStroke(inputCtx.value, newX, newY, width, strokes[`${strokeKey}`].style.color, true)
        // } else if (options.value === Actions.erase) {
        //     drawErase(x, y, width)
        // }
        // strokes[`${strokeKey}`].points.push({x: x - translatePos.value.x, y: y - translatePos.value.y})




        // console.log(strokes[`${strokeKey}`].style.width / scale.value)
        // const newX = Math.floor((x - translatePos.value.x) / scale.value) * scale.value
        // const newY = Math.floor((y - translatePos.value.y) / scale.value) * scale.value
        // const width = strokes[`${strokeKey}`].style.width
        // if (options.value === Actions.add || options.value === Actions.remove) {
        //     drawStroke(inputCtx.value, newX, newY, width, strokes[`${strokeKey}`].style.color, true)
        // } else if (options.value === Actions.erase) {
        //     drawErase(x, y, width)
        // }


        const width = strokes[`${strokeKey}`].style.width
        if (options.value === Actions.add || options.value === Actions.remove) {
            drawStroke(inputCtx.value, x, y, width, strokes[`${strokeKey}`].style.color, true)
        } else if (options.value === Actions.erase) {
            drawErase(x, y, width)
        }
    }

    return { drawMouseDownListener, drawMouseUpListener, drawMouseMoveListener, drawMouseOverListener, drawMouseOutListener, redrawOnImageTranslated };
}
