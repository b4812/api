import {ref} from 'vue'
import type { Ref } from 'vue';

interface Plane {
    x: number;
    y: number;
}

export default function useDrawImageInCanvas(
    inputCanvas: Ref<HTMLCanvasElement | null>,
    inputCtx: Ref<CanvasRenderingContext2D | null>,
    outputCanvas: Ref<HTMLCanvasElement | null>,
    outputCtx: Ref<CanvasRenderingContext2D | null>,
    img: Ref<HTMLImageElement | null>,
    imgEdited: Ref<HTMLImageElement | null>,
) {
    const translatePos = ref({x: 0, y: 0}) 
    const startDragOffset = ref({x: 0, y: 0})
    const scale = ref(1)

    function drawImageInCanvas(canvas: Ref<HTMLCanvasElement | null>, ctx: Ref<CanvasRenderingContext2D | null>, img: Ref<HTMLImageElement | null>) {
        if (!(canvas.value && ctx.value && img.value)) return;

        ctx.value.clearRect(0, 0, canvas.value.width, canvas.value.height);
        ctx.value.save();
        ctx.value.translate(translatePos.value.x, translatePos.value.y);
        ctx.value.scale(scale.value, scale.value);
        ctx.value.drawImage(img.value, 0, 0, img.value.width, img.value.height);
        ctx.value.restore();
    }

    function drawImage() {
        drawImageInCanvas(inputCanvas, inputCtx, img);
        drawImageInCanvas(outputCanvas, outputCtx, imgEdited);
    }

    return {
        drawImage,
        translatePos,
        startDragOffset,
        scale
    };
}
