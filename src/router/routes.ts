import Home from '@/views/Home.vue'
import Main from '@/views/Main.vue'
export default [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
    path: '/main',
    name: 'main',
    component: Main
  }
]
