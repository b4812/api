import type { Ref } from 'vue';

interface Plane {
    x: number;
    y: number;
}
export default function useTranslateImages(
    drawImage: () => void,
    redrawOnImageTranslated: () => void,
    inputCtx: Ref<CanvasRenderingContext2D | null>,
    outputCtx: Ref<CanvasRenderingContext2D | null>,
    inputCanvas: Ref<HTMLCanvasElement | null>,
    outputCanvas: Ref<HTMLCanvasElement | null>,
    img: Ref<HTMLImageElement | null>,
    translatePos: Ref<Plane>,
    scale: Ref<number>,
) {
    let mouseDownZoomIn = false;
    let mouseDownZoomOut = false;

    async function zoomInMouseDownListener() {
        mouseDownZoomIn = true;
        while (mouseDownZoomIn) {
            await new Promise((resolve, reject) => {
                setTimeout(() => {
                    if (!(inputCtx.value && outputCtx.value && img.value && inputCanvas.value && outputCanvas.value)) {
                        reject('Unknown elements');
                        return;
                    } else if (scale.value >= 70) {
                        scale.value = 70;
                        reject('scale.value value exceeded');
                        return;
                    }
                    const scaleValue = (scale: number) => scale + scale / 30;
                    translatePos.value.x -= (inputCanvas.value.width / 2 - translatePos.value.x) * (scaleValue(scale.value) / scale.value - 1);
                    translatePos.value.y -= (inputCanvas.value.height / 2 - translatePos.value.y) * (scaleValue(scale.value) / scale.value - 1);
                    scale.value = scaleValue(scale.value);
                    drawImage();
                    redrawOnImageTranslated()
                    resolve(' ');
                }, 1);
            });
        }
    }
    function zoomInMouseUpListener() {
        mouseDownZoomIn = false;
    }
    function zoomInMouseMoveListener() {
        mouseDownZoomIn = false;
    }

    async function zoomOutMouseDownListener() {
        mouseDownZoomOut = true;
        while (mouseDownZoomOut) {
            await new Promise((resolve, reject) => {
                setTimeout(() => {
                    const scaleValue = (scale: number) => scale - scale / 30;
                    if (!(inputCtx.value && outputCtx.value && img.value && inputCanvas.value && outputCanvas.value)) {
                        reject('Unknown elements');
                        return;
                    } else if (img.value.width * scale.value <= inputCanvas.value.width / 4) {
                        scale.value = inputCanvas.value.width / (4 * img.value.width);
                        reject('Maz zoom reached');
                        return;
                    }
                    translatePos.value.x += (inputCanvas.value.width / 2 - translatePos.value.x) * (1 - scaleValue(scale.value) / scale.value);
                    translatePos.value.y += (inputCanvas.value.height / 2 - translatePos.value.y) * (1 - scaleValue(scale.value) / scale.value);
                    scale.value = scaleValue(scale.value);
                    drawImage();
                    redrawOnImageTranslated()
                    resolve(' ');
                }, 1);
            });
        }
    }
    function zoomOutMouseUpListener() {
        mouseDownZoomOut = false;
    }
    function zoomOutMouseMoveListener() {
        mouseDownZoomOut = false;
    }

    return {
        zoomInMouseDownListener,
        zoomInMouseMoveListener,
        zoomInMouseUpListener,
        zoomOutMouseDownListener,
        zoomOutMouseMoveListener,
        zoomOutMouseUpListener
    }
}
