import { createApp } from 'vue';
import { createPinia } from 'pinia';

import { Quasar } from 'quasar';
import 'quasar/src/css/index.sass';
import '@quasar/extras/eva-icons/eva-icons.css'
import '@quasar/extras/fontawesome-v5/fontawesome-v5.css'
import './tailwind.scss'

import App from './App.vue';
import router from './router';

const app = createApp(App);

app.use(Quasar, {
    plugins: {},
});
app.use(createPinia());
app.use(router);

app.mount('#app');
